// if statement
/* 
    -executes a statement if a specified condition is true
    -can standalone without the else statement

    Syntax:
        if(condition) {
            statement/code block
        }
*/
let numA = -1;

if (numA < 0 ) {
    console.log(numA + " is less than 0");
}

console.log(numA < 0)

if (numA > 0){
    console.log("This statement will not be printed.")
}

let city = "Los Angeles City";

if (city === "Los Angeles City"){
    console.log("Welcome to " + city + "!");
}

// else if clause
/* 
    -executes a statement if previous conditions are false and if the specified condition is true
    -"else if" clause is optional and can be added to capture additional conditions to change the flow of a program
*/
if (city === "New York City") {
    console.log("Welcome to " + city + "!");
} 
else if (city === "Los Angeles City") {
    console.log("Welcome to " + city + "!");
}

let numB = 1;

if (numA > 0) {
    console.log("Hello");
} else if (numB > 0) {
    console.log("World");
}

city = "Tokyo";

if (city === "New York City") {
    console.log("Welcome to " + city + "!");
} else if (city === "Tokyo") {
    console.log("Welcome to " + city + "!");
}

// else statement
/* 
    -executes a statement if all other conditions are falls
    -the "else" statement is also optional and can be added to capture any other result to change the flow of a program
*/

if (numA > 0) {
    console.log("Hello");
} else if (numB === 0) {
    console.log("World");
} else {
    console.log("Again");
}

/* let age = prompt("Enter your age!");
if (age <= 18) {
    console.log("Oops! Mag juice ka muna!");
} else {
    console.log("Matanda ka na, shot na!");
} */


// mini-activity
/* let height = prompt("Enter your height in cm:");

function checkHeight(height) {
    if (height >= 150) {
        console.log("Passed minimun height requirement.");
    } else {
        console.log("Did not pass minimum requirement.");
    }
};

checkHeight(height); */

let message = "No message.";
// console.log(message);

function determineTyphoonIntensity(windSpeed) {
    if (windSpeed < 30) {
        return 'Not a typhoon yet.';
    } else if (windSpeed <= 61) {
        return 'Tropical depression detected.';
    } else if (windSpeed >= 62 && windSpeed <= 88) {
        return 'Tropical storm detected.';
    } else if (windSpeed >= 89 && windSpeed <= 117) {
        return 'Severe tropical strom detected.';
    } else {
        return 'Typhoon detected.'
    }
}

message = determineTyphoonIntensity(63);
console.log(message);

if (message === "Tropical storm detected.") {
    console.warn(message);
}


// Truthy and Falsy values
/* 
    -In JavaScript, a truthy value is a value that is considered true when encountered in a boolean context
    -Values arae considered true unless defined otherwise

    Falsy/exceptions for truthy
    1. false
    2. 0
    3. -0
    4. ""
    5. null
    6. undefined
    7. NaN
*/

// Truthy Examples
if (true) {
    console.log("Truthy!");
}

if (1) {
    console.log("Another truthy!");
}

if ([]) {
    console.log("Another another truthy!");
}

// Falsy Examples
if (false) {
    console.log("Falsy!");
}

if (0) {
    console.log("Falsy!");
}

if (-0) {
    console.log("Falsy!");
}

if (null) {
    console.log("Falsy!");
}

if (undefined) {
    console.log("Falsy!");
}

if (NaN) {
    console.log("Falsy!");
}

// Conditional (Ternary) Operator
/* 
    Ternary operator takes in three operands
    1. condition
    2. expression to execute if the condition is truthy
    3. expression to executed if the condition is falsy

    Syntaxt:
        (condition) ? ifTrue : ifFalse
*/

//Single Statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

/* let firstName;

function isOfLegalAge() {
    firstName = 'John';
    return "You are of the legal age limit";
}

function isOfUnderAge() {
    firstName = 'Jane';
    return "You are of under the age limit";
}

let age = parseInt(prompt("What is your age?"));
console.log(age);
console.log(typeof age);
let legalAge = (age > 18) ? isOfLegalAge() : isOfUnderAge();
console.log(legalAge + ", " + firstName); */


// Switch Statement
/* 
    Syntax:
        switch (expression) {
            case value :
                statement;
                break;
            default:
                statement;
                break;
        }
*/

/* let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day)

switch (day) {
    case 'monday':
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;    
    case 'sunday':
        console.log("The color of the day is violet");
        break;   
    default:
        console.log("Please input a valid day!");
        break;
} */


// Try-Catch-Finally Statement
function showIntensityAlert(windSpeed) {
    try {
        //attempt to execute a code
        alerat(determineTyphoonIntensity(windSpeed));
    }

    catch (error) {
        //catch errors within "try" statement
        console.log(error);
        console.warn(error.message);
    }

    finally {
        //continue execution of code regardless of success or failure of code execution in the "try" block 
        //alert("Intensity updates will show new alert.");
    }
}

showIntensityAlert(56);
