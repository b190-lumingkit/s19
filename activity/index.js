let username;
let password;
let role;

// login function, get user data -> username, password, role
function userLogin() {
    username = prompt("Enter your username:");
    password = prompt("Enter your password:");
    role = String(prompt("Enter your role:")).toLowerCase();
    
    if ((username === "" || username === null) || (password === "" || password === null) || (role === "" || role === null)) {
        alert("Input must not be empty");
    } else {
        switch (role) {
            case "admin":
                alert("Welcome back to the class portal, admin!");
                break;
            case "teacher":
                alert("Thank you for logging in, teacher!");
                break;
            case "student":
            case "rookie":
                alert("Welcome to the class portal, student!");
                break;
            default:
                alert("Role out of range");
                break;
        }
    }
}
//call the userLogin function
userLogin();

// function in checking the average of four numbers
function checkAverage(grade1, grade2, grade3, grade4) {
    let average = (grade1 + grade2 + grade3 + grade4) / 4;
    average = Math.round(average);
    console.log(average);

    if (average <= 74) {
        console.log("Hello, " + role + "," + " your average is " + average + "." + " The letter equivalent is F");
    } else if (average >= 75 && average <= 79) {
        console.log("Hello, " + role + "," + " your average is " + average + "." + " The letter equivalent is D");
    } else if (average >= 80 && average <= 84) {
        console.log("Hello, " + role + "," + " your average is " + average + "." + " The letter equivalent is C");
    } else if (average >= 85 && average <= 89) {
        console.log("Hello, " + role + "," + " your average is " + average + "." + " The letter equivalent is B");
    } else if (average >= 90 && average <= 95) {
        console.log("Hello, " + role + "," + " your average is " + average + "." + " The letter equivalent is A");
    } else if (average >= 96) {
        console.log("Hello, " + role + "," + " your average is " + average + "." + " The letter equivalent is A+");
    }
}